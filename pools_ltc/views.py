from itertools import chain
from operator import attrgetter
from django.views.generic import ListView
from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from .models import BTC, EMCD, F2, POOLIN, TRUSTPOOL, VIABTC, LITECOINPOOL, SIGMAPOOL

from .serializers import BTCSerializer, BTCSerializerPub, EMCDSerializer,\
    EMCDSerializerPub, F2Serializer, F2SerializerPub, POOLINSerializer,\
    POOLINSerializerPub, TRUSTPOOLSerializer, TRUSTPOOLSerializerPub,\
    VIABTCSerializer, VIABTCSerializerPub, LITECOINPOOLSerializerPub,\
    LITECOINPOOLSerializer, SIGMAPOOLSerializer, SIGMAPOOLSerializerPub


class index(ListView):
    template_name = 'pool_list_ltc.html'
    context_object_name = 'poolslist_ltc'


    def get_queryset(self):
        qs1 = BTC.objects.all().order_by('-published')[:1]
        qs2 = EMCD.objects.all().order_by('-published')[:1]
        qs3 = F2.objects.all().order_by('-published')[:1]
        qs4 = POOLIN.objects.all().order_by('-published')[:1]
        qs5 = TRUSTPOOL.objects.all().order_by('-published')[:1]
        qs6 = VIABTC.objects.all().order_by('-published')[:1]
        qs7 = LITECOINPOOL.objects.all().order_by('-published')[:1]
        qs8 = SIGMAPOOL.objects.all().order_by('-published')[:1]
        queryset = sorted(chain(
            qs1, qs2, qs3, qs4, qs5, qs6, qs7, qs8),
                          key=attrgetter('price'), reverse=True)
        return queryset


class api_ltc_all_pool_view_set(viewsets.ModelViewSet):
    serializer_class = (BTCSerializerPub)
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        qs1 = BTC.objects.all().order_by('-published')[:1]
        qs2 = EMCD.objects.all().order_by('-published')[:1]
        qs3 = F2.objects.all().order_by('-published')[:1]
        qs4 = POOLIN.objects.all().order_by('-published')[:1]
        qs5 = TRUSTPOOL.objects.all().order_by('-published')[:1]
        qs6 = VIABTC.objects.all().order_by('-published')[:1]
        qs7 = LITECOINPOOL.objects.all().order_by('-published')[:1]
        qs8 = SIGMAPOOL.objects.all().order_by('-published')[:1]
        queryset = sorted(chain(
            qs1, qs2, qs3, qs4, qs5, qs6, qs7, qs8),
                          key=attrgetter('price'), reverse=True)
        return queryset


class api_btc_ltc_view_set(viewsets.ModelViewSet):
    queryset = BTC.objects.all().order_by('-published')
    serializer_class = BTCSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_btc_ltc_pub(viewsets.ModelViewSet):
    model = BTC.objects.all().order_by('-published')
    serializer_class = BTCSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return BTC.objects.all().order_by('-published')


class api_emcd_ltc_view_set(viewsets.ModelViewSet):
    queryset = EMCD.objects.all().order_by('-published')
    serializer_class = EMCDSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_emcd_ltc_pub(viewsets.ModelViewSet):
    model = EMCD.objects.all().order_by('-published')
    serializer_class = EMCDSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return EMCD.objects.all().order_by('-published')


class api_f2_ltc_view_set(viewsets.ModelViewSet):
    queryset = F2.objects.all().order_by('-published')
    serializer_class = F2Serializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_f2_ltc_pub(viewsets.ModelViewSet):
    model = F2.objects.all().order_by('-published')
    serializer_class = F2SerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return F2.objects.all().order_by('-published')


class api_poolin_ltc_view_set(viewsets.ModelViewSet):
    queryset = POOLIN.objects.all().order_by('-published')
    serializer_class = POOLINSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_poolin_ltc_pub(viewsets.ModelViewSet):
    model = POOLIN.objects.all().order_by('-published')
    serializer_class = POOLINSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return POOLIN.objects.all().order_by('-published')


class api_trustpool_ltc_view_set(viewsets.ModelViewSet):
    queryset = TRUSTPOOL.objects.all().order_by('-published')
    serializer_class = TRUSTPOOLSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_trustpool_ltc_pub(viewsets.ModelViewSet):
    model = TRUSTPOOL.objects.all().order_by('-published')
    serializer_class = TRUSTPOOLSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return TRUSTPOOL.objects.all().order_by('-published')


class api_viabtc_ltc_view_set(viewsets.ModelViewSet):
    queryset = VIABTC.objects.all().order_by('-published')
    serializer_class = VIABTCSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_viabtc_ltc_pub(viewsets.ModelViewSet):
    model = VIABTC.objects.all().order_by('-published')
    serializer_class = VIABTCSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return VIABTC.objects.all().order_by('-published')


class api_litecoinpool_ltc_view_set(viewsets.ModelViewSet):
    queryset = LITECOINPOOL.objects.all().order_by('-published')
    serializer_class = LITECOINPOOLSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_litecoinpool_ltc_pub(viewsets.ModelViewSet):
    model = LITECOINPOOL.objects.all().order_by('-published')
    serializer_class = LITECOINPOOLSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return LITECOINPOOL.objects.all().order_by('-published')


class api_sigmapool_ltc_view_set(viewsets.ModelViewSet):
    queryset = SIGMAPOOL.objects.all().order_by('-published')
    serializer_class = SIGMAPOOLSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_sigmapool_ltc_pub(viewsets.ModelViewSet):
    model = SIGMAPOOL.objects.all().order_by('-published')
    serializer_class = SIGMAPOOLSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return SIGMAPOOL.objects.all().order_by('-published')
