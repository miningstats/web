from django.apps import AppConfig


class PoolsLtcConfig(AppConfig):
    name = 'pools_ltc'
