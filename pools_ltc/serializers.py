from rest_framework import serializers
from .models import BTC, EMCD, F2, POOLIN, TRUSTPOOL, VIABTC, LITECOINPOOL, SIGMAPOOL


class BTCSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BTC
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')


class BTCSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = BTC
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')


class EMCDSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EMCD
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')


class EMCDSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = EMCD
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')


class F2Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = F2
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')


class F2SerializerPub(serializers.ModelSerializer):
    class Meta:
        model = F2
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')


class POOLINSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = POOLIN
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')


class POOLINSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = POOLIN
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')


class TRUSTPOOLSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TRUSTPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')


class TRUSTPOOLSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = TRUSTPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')


class VIABTCSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = VIABTC
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')


class VIABTCSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = VIABTC
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')

class LITECOINPOOLSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = LITECOINPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')


class LITECOINPOOLSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = LITECOINPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')

class SIGMAPOOLSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SIGMAPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')


class SIGMAPOOLSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = SIGMAPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'doge')
