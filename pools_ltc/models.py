from django.db import models

class BTC(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )
    doge = models.CharField(
        max_length=99999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'BTC_com LTC'
        verbose_name = 'BTC_com LTC'
        ordering = ['-price', '-published']

class EMCD(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )
    doge = models.CharField(
        max_length=99999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'EMCD LTC'
        verbose_name = 'EMCD LTC'
        ordering = ['-price', '-published']

class F2(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )
    doge = models.CharField(
        max_length=99999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'F2 LTC'
        verbose_name = 'F2 LTC'
        ordering = ['-price', '-published']

class POOLIN(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )
    doge = models.CharField(
        max_length=99999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'POOLIN LTC'
        verbose_name = 'POOLIN LTC'
        ordering = ['-price', '-published']


class TRUSTPOOL(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )
    doge = models.CharField(
        max_length=99999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'TRUSTPOOL LTC'
        verbose_name = 'TRUSTPOOL LTC'
        ordering = ['-price', '-published']


class VIABTC(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )
    doge = models.CharField(
        max_length=99999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'VIABTC LTC'
        verbose_name = 'VIABTC LTC'
        ordering = ['-price', '-published']


class LITECOINPOOL(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )
    doge = models.CharField(
        max_length=99999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'LITECOINPOOL'
        verbose_name = 'LITECOINPOOL'
        ordering = ['-price', '-published']


class SIGMAPOOL(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )
    doge = models.CharField(
        max_length=99999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'SIGMAPOOL LTC'
        verbose_name = 'SIGMAPOOL LTC'
        ordering = ['-price', '-published']
