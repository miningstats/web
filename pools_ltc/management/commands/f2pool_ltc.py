from django.core.management.base import BaseCommand
from django.conf import settings
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
import time
import requests
import base64
from decimal import Decimal


class Command(BaseCommand):
    help = 'Parse viabtc pool calc coin'

    def ltc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://www.f2pool.com/coin/litecoin")
        scheight = .1
        while scheight < 2.5:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'f2pool_ltc.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        f2calcresult = soup.find("span", {"class": "pl-1 profit-val"}).text
        f2_btc_regex = f2calcresult.replace(" ", "").replace(",", "")[1:][:-1]
        f2_convert_mh = Decimal(f2_btc_regex) / Decimal(1000)
        f2_round_fix = round(f2_convert_mh, 8)
        print(f2_round_fix)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'f2pool_ltc.png')
        namepool = "F2POOL"
        urlpool = "https://www.f2pool.com/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/ltc/f2pool/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': f2_round_fix, "price_check_image": imgbburl, "doge":"0"}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'f2pool_ltc.png'))
        except Exception:
            pass


    def handle(self, *args, **kwargs):
        self.ltc()
