from django.core.management.base import BaseCommand
from django.conf import settings
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
import time
import requests
import base64
from decimal import Decimal


class Command(BaseCommand):
    help = 'Parse viabtc pool calc coin'

    def ltc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        #cap = DesiredCapabilities().FIREFOX.copy()
        #cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://www.poolin.com")
        time.sleep(15)
        elem = driver.find_element_by_class_name("know-btn")
        elem.click()
        time.sleep(5)
        elem = driver.find_element_by_class_name("coin-name-container")
        elem.click()
        time.sleep(5)
        elem = driver.find_element_by_xpath("//img[@src='https://s.blockin.com/pool-assets/coin_icon/circle/ltc.svg']")
        elem.click()
        scheight = .1
        while scheight < 11.9:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .05
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'poolin_ltc.png'))
        time.sleep(5)
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        poolincalcresult = soup.find("span", {"class": "income-result"})
        list_of_inner_text = [x.text for x in poolincalcresult]
        poolintext = ', '.join(list_of_inner_text)
        poolin_convert_mh = Decimal(poolintext) / Decimal(1000)
        poolin_round_fix = round(poolin_convert_mh, 8)
        poolin_fee_min = Decimal(poolin_convert_mh) * Decimal(1 - 3/100)
        poolin_fee_min_fix = round(poolin_fee_min, 8)
        print(poolin_fee_min_fix)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'poolin_ltc.png')
        namepool = "POOLIN"
        urlpool = "https://www.poolin.com/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/ltc/poolin/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': poolin_fee_min_fix, "price_check_image": imgbburl, "doge":"0"}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'poolin_ltc.png'))
        except Exception:
            pass

    def handle(self, *args, **kwargs):
        self.ltc()
