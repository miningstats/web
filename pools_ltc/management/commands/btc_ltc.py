from django.core.management.base import BaseCommand
from django.conf import settings
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
import time
import requests
import base64
from decimal import Decimal


class Command(BaseCommand):
    help = 'Parse viabtc pool calc coin'

    def ltc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://pool.btc.com/")
        time.sleep(5)
        elem = driver.find_element_by_xpath("//img[@src='/static/images/coin/icon_ltc@2x.png']")
        elem.click()
        scheight = .1
        while scheight < 9.9:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'btc_ltc.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        btccalcresult = soup.findAll("span", {"class": "coin-list__detail__earning coin-list__detail__earning_coin"})
        list_of_inner_text = [x.text for x in btccalcresult]
        btctext = "".join(list_of_inner_text)
        btc_regex = btctext.replace(" ", "").replace("LTC", "")

        # https://help.pool.btc.com/hc/en-us/articles/360022197472-Continue-Preferential-Fee-for-LTC-Mining-and-DOGE-Distribution-in-BTC-com-Pool
        # mining_pay + ~7.5%_DOGE - 3%_fee

        btc_fee_min = Decimal(btc_regex) * Decimal(1 + 7.5/100) * Decimal(1 - 3/100)
        btc_fee_min_fix = round(btc_fee_min, 8)
        print(btc_fee_min_fix)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'btc_ltc.png')
        namepool = "BTCcom"
        urlpool = "https://pool.btc.com/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/ltc/btccom/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': btc_fee_min_fix, "price_check_image": imgbburl, "doge":"~7.5%"}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'btc_ltc.png'))
        except Exception:
            pass


    def handle(self, *args, **kwargs):
        self.ltc()
