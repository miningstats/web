from django.core.management.base import BaseCommand
from django.conf import settings
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
import requests
import base64
from decimal import Decimal


class Command(BaseCommand):
    help = 'Parse litecoinpool calc ltc'

    def ltc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://www.litecoinpool.org/calc?hashrate=1&speedunit=MH")
        scheight = .1
        while scheight < 7.3:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .05
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'litecoinpool.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        litecoinpoolcalcresult = soup.findAll("td", {"class": "value"})
        list_of_inner_text = [x.text for x in litecoinpoolcalcresult]
        litecoinpooltext = ', '.join(list_of_inner_text)
        litecoinpool_regex = litecoinpooltext.replace(" ", "").replace(",", "")[20:-87]
        print(litecoinpool_regex)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'litecoinpool.png')
        namepool = "LITECOINPOOL"
        urlpool = "https://litecoinpool.org/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/ltc/litecoinpool/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': litecoinpool_regex, "price_check_image": imgbburl, "doge":"0"}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'litecoinpool.png'))
        except Exception:
            pass


    def handle(self, *args, **kwargs):
        self.ltc()
