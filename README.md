Install python3.7

```
virtualenv env
source env/bin/activate
pip install -r requirements.txt
python manage.py makemigrations pools
python manage.py makemigrations pools_eth
python manage.py makemigrations pools_ltc
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver

```

create POOLS_TOKEN_AUTO admin and add settings

![](token_api.png)

add API_KEY_IMGBB settings

run python manage.py binance

support pools btc

```
[pools]
    binance
    btc
    emcd
    expool
    f2pool
    poolin
    prohashing
    rupoolpro
    sigmapool
    trustpool
    ukrpool
    viabtc

```

support pools eth

```
[pools_eth]
    btc_com_eth
    emcd_eth
    f2pool_eth
    minerall_eth
    poolin_eth
    viabtc_eth

```

support pools ltc

```
[pools_ltc]
    btc_ltc
    emcd_ltc
    f2pool_ltc
    litecoinpool_ltc
    poolin_ltc
    sigmapool_ltc
    trustpool_ltc
    viabtc_ltc

```

load old data

```
python manage.py loaddata dumpdata/pools.json

```
