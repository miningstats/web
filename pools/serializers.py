from rest_framework import serializers
from .models import Binance, BTC, EMCD, EXPOOL, EXPOOLAB, F2, POOLIN,\
    PROHASHING, RUPOOLPRO, SIGMAPOOL, TRUSTPOOL, UKRPOOL, VIABTC, btcmachinelearning


class BinanceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Binance
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class BinanceSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = Binance
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class BTCSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BTC
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class BTCSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = BTC
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class EMCDSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EMCD
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class EMCDSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = EMCD
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class EXPOOLSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EXPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class EXPOOLSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = EXPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class EXPOOLABSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EXPOOLAB
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class EXPOOLABSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = EXPOOLAB
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class F2Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = F2
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class F2SerializerPub(serializers.ModelSerializer):
    class Meta:
        model = F2
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class POOLINSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = POOLIN
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class POOLINSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = POOLIN
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class PROHASHINGSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PROHASHING
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class PROHASHINGSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = PROHASHING
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class RUPOOLPROSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = RUPOOLPRO
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class RUPOOLPROSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = RUPOOLPRO
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class SIGMAPOOLSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SIGMAPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class SIGMAPOOLSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = SIGMAPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class TRUSTPOOLSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TRUSTPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class TRUSTPOOLSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = TRUSTPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class UKRPOOLSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UKRPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class UKRPOOLSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = UKRPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class VIABTCSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = VIABTC
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class VIABTCSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = VIABTC
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class btcmachinelearningSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = btcmachinelearning
        fields = ('url',)


class btcmachinelearningSerializer(serializers.ModelSerializer):
    class Meta:
        model = btcmachinelearning
        fields = ('url',)
