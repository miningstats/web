from django.urls import path
from .views import index, rupool_detali, binance_detali, btccom_detali,\
    emcd_detali, f2_detali, poolin_detali, prohashing_detali,\
    sigmapool_detali, trustpool_detali, ukrpool_detali, viabtc_detali

urlpatterns = [
    path('', index.as_view()),
    path('rupoolpro/', rupool_detali.as_view()),
    path('binance/', binance_detali.as_view()),
    path('btc.com/', btccom_detali.as_view()),
    path('emcd/', emcd_detali.as_view()),
    path('f2pool/', f2_detali.as_view()),
    path('poolin/', poolin_detali.as_view()),
    path('prohashing/', prohashing_detali.as_view()),
    path('sigmapool/', sigmapool_detali.as_view()),
    path('trustpool/', trustpool_detali.as_view()),
    path('okkong/ukrpool/', ukrpool_detali.as_view()),
    path('viabtc/', viabtc_detali.as_view()),
]
