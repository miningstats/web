from django.db import models


class Binance(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'BINANCE'
        verbose_name = 'BINANCE'
        ordering = ['-price', '-published']


class BTC(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'BTC'
        verbose_name = 'BTC'
        ordering = ['-price', '-published']


class EMCD(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'EMCD'
        verbose_name = 'EMCD'
        ordering = ['-price', '-published']


class EXPOOL(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'EXPOOLNOAB'
        verbose_name = 'EXPOOLNOAB'
        ordering = ['-price', '-published']


class EXPOOLAB(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'EXPOOLAB'
        verbose_name = 'EXPOOLAB'
        ordering = ['-price', '-published']


class F2(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'F2'
        verbose_name = 'F2'
        ordering = ['-price', '-published']


class POOLIN(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'POOLIN'
        verbose_name = 'POOLIN'
        ordering = ['-price', '-published']


class PROHASHING(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'PROHASHING'
        verbose_name = 'PROHASHING'
        ordering = ['-price', '-published']


class RUPOOLPRO(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'RUPOOLPRO'
        verbose_name = 'RUPOOLPRO'
        ordering = ['-price', '-published']


class SIGMAPOOL(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'SIGMAPOOL'
        verbose_name = 'SIGMAPOOL'
        ordering = ['-price', '-published']


class TRUSTPOOL(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'TRUSTPOOL'
        verbose_name = 'TRUSTPOOL'
        ordering = ['-price', '-published']


class UKRPOOL(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'UKRPOOL'
        verbose_name = 'UKRPOOL'
        ordering = ['-price', '-published']


class VIABTC(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'VIABTC'
        verbose_name = 'VIABTC'
        ordering = ['-price', '-published']


class btcmachinelearning(models.Model):
    url = models.CharField(
        max_length=9999,
    )

    def __str__(self):
       	return self.url

    class Meta:
        verbose_name_plural = 'btcmachinelearning'
       	verbose_name = 'btcmachinelearning'
