
from django.core.management.base import BaseCommand
from django.conf import settings
from PIL import Image
import cv2
import os
import requests
import base64
import pytesseract


class Command(BaseCommand):
    help = 'Parse rupoolpro calc coin'

    def btc(self):
        url = 'https://rupool.pro/profit.php'
        r = requests.get(url)
        with open('profit.php', 'wb') as f:
            f.write(r.content)
        os.rename('profit.php', os.path.join(settings.BASE_DIR, 'rupoolpro.png'))
        image = cv2.imread(os.path.join(settings.BASE_DIR, 'rupoolpro.png'))
        cropped = image[115:275, 1385:-105]
        grayImage = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY)
        os.path.join(settings.BASE_DIR, 'thumbnail.jpg')
        cv2.imwrite(os.path.join(settings.BASE_DIR, 'thumbnail.jpg'), grayImage)
        rupooltext = pytesseract.image_to_string(Image.open(os.path.join(settings.BASE_DIR, 'thumbnail.jpg'))).replace(" ", "")[:-7]
        print(rupooltext)
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'rupoolpro.png')
        namepool = "RUPOOLPRO"
        urlpool = "https://rupool.pro/en/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/btc/rupoolpro/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': rupooltext, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'rupoolpro.png'))
            os.remove(os.path.join(settings.BASE_DIR, 'thumbnail.jpg'))
            os.remove(os.path.join(settings.BASE_DIR, 'thumbnail2.jpg'))
        except Exception:
            pass

    def handle(self, *args, **kwargs):
        self.btc()
