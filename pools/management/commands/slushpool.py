# pplns profits calculated formula
# search blockchain block date and sum reward
# api search
# https://api.blockchair.com/bitcoin/blocks?q=time(2020-10-16),guessed_miner(SlushPool)&s=size(desc)&limit=10
# sum all reward
# 2020-10-16 ~25
#            Hashrate / Pool Hashrate * Reward * (1 - Pool Fee)
# 2020-10-16   1          3211000        25          2%
# Decimal(1) / Decimal(3211000) *  Decimal(25) * Decimal(1 - 2/100)
from django.core.management.base import BaseCommand
from django.conf import settings
from datetime import datetime, timedelta
import requests
import numpy as np
from decimal import Decimal
import json


class Command(BaseCommand):
    help = 'Parse slushpool calc coin'

    def btc(self):
        date_convert = datetime.now() - timedelta(days=1)
        date_convert = date_convert.strftime('%Y-%m-%d')
        url_constructing = 'https://api.blockchair.com/bitcoin/blocks?q=time('
        url_constructing2 = date_convert
        url_constructing3 = '),guessed_miner(SlushPool)&s=size(desc)&limit=10'
        url_constructing_full = url_constructing + url_constructing2 + url_constructing3
        remove_api_url = url_constructing_full.replace("api.", "")
        url_constructing_full_no_api = remove_api_url
        url_get_block = requests.get(url_constructing_full)
        url_get_block_check = url_get_block.json()["context"]["rows"]
        block_check = float(url_get_block_check)
        if block_check > 0:
            get_block = url_get_block.json()["data"]
            for row in get_block[:1]:
                prices = [x["reward"] for x in get_block]
                prices = np.array(prices).astype(np.float)
                sum_prices = np.sum(prices)
                sum_prices_round_fix = (str(Decimal(sum_prices))[:10])
                sum_prices_round_fix = float(sum_prices_round_fix)
                sum_prices_round_fix = float(sum_prices_round_fix / 100000000)

            reward_sum = sum_prices_round_fix
            slush_token = settings.SLUSH_TOKEN
            token = {'SlushPool-Auth-Token': slush_token}
            hash_rate = requests.get('https://slushpool.com/stats/json/btc/', headers={"SlushPool-Auth-Token": slush_token})
            hash_rate = hash_rate.json()["btc"]["pool_scoring_hash_rate"]
            hash_rate = str(hash_rate)[:7]
            calc_profit = Decimal(1) / Decimal(hash_rate) *  Decimal(reward_sum) * Decimal(1 - 2/100)
            calc_profit_round = str(calc_profit)[:10]
            print(calc_profit_round)
            remove_api_url = url_constructing_full.replace("api.", "")
            auth_token = settings.POOLS_TOKEN_AUTO
            namepool = "SlushPool"
            urlpool = "https://slushpool.com/"
            hed = {'Authorization': 'Token ' + auth_token}
            url_api_path = '/api/btc/slushpool/post/?format=json'
            url = settings.SITE_URL + url_api_path
            payload = {'name': namepool, 'url': urlpool, 'price': calc_profit_round, "price_check_image": url_constructing_full_no_api}
            r = requests.get(url, headers=hed)
            r = requests.get(url, params=payload, headers=hed)
            r = requests.post(url, data=payload, headers=hed)

        elif block_check == 0:
            calc_profit_round = "0.00000000"
            print(calc_profit_round)
            auth_token = settings.POOLS_TOKEN_AUTO
            namepool = "SlushPool"
            urlpool = "https://slushpool.com/"
            hed = {'Authorization': 'Token ' + auth_token}
            url_api_path = '/api/btc/slushpool/post/?format=json'
            url = settings.SITE_URL + url_api_path
            payload = {'name': namepool, 'url': urlpool, 'price': calc_profit_round, "price_check_image": url_constructing_full_no_api}
            r = requests.get(url, headers=hed)
            r = requests.get(url, params=payload, headers=hed)
            r = requests.post(url, data=payload, headers=hed)
        else:
            print("Errr")


    def handle(self, *args, **kwargs):
        self.btc()
