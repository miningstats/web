from django.core.management.base import BaseCommand
from django.conf import settings
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
import requests
import base64
import time
from decimal import Decimal


class Command(BaseCommand):
    help = 'Parse viabtc pool calc coin'

    def btc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        time.sleep(10)
        driver.implicitly_wait(10)
        driver.get("https://ukrpool.com/")
        scheight = .1
        while scheight < 4.0:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'ukrpool.png'))
        time.sleep(10)
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        ukrpoolcalcresult = soup.find("ul", {"class": "table-header bg"}).text
#        list_of_inner_text = [x.text for x in ukrpoolcalcresult]
#        ukrpooltext = ', '.join(list_of_inner_text)
        ukrpool_regex = ukrpoolcalcresult.replace(" ", "").replace("/", "").replace("$", "")[26:-17]
#       ukrpool_regex = ukrpoolcalcresult.replace(" ", "").replace("/", "").replace("$", "")[24:][:-423]
        print(ukrpool_regex)
        ukrpool_fee_min = Decimal(ukrpool_regex) * Decimal(1 - 4/100)
        ukrpool_fee_min_fix = round(ukrpool_fee_min, 8)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'ukrpool.png')
        namepool = "OKKONG/UKRPOOL"
        urlpool = "https://ukrpool.com/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path= '/api/btc/ukrpool/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': ukrpool_fee_min_fix, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'ukrpool.png'))
        except Exception:
            pass

    def ltc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        time.sleep(10)
        driver.implicitly_wait(10)
        driver.get("https://ukrpool.com/")
        time.sleep(5)
        elem = driver.find_element_by_xpath("//img[@src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAEyElEQVRoQ91abWhbVRh+TpqalDbasjGsiiZSFLd1ZhMhmUMLiiJDbP3qH2HZUH/4QeMP24joWkXM+mcRxR9OXLZ/okJFpoiK8QObP1uzpbRMZImgmwyk1bQktSOvnJvcNF/345x7G4IXCqV9z/M+zznvfd/3nHMZbHj84Uyvu2v1YRB5AQwB5GVg/PfKQ6AswLIAEmAsW8h3f5aK+ZatumeyAJy0y7VywAGEwOCXwiGkikB8ba3nhKwYYQGByKKX0foYFOKsV4p4/SCiZWIsBjhPJKO38VUy/QgJCETShxlR2DbiGkKS0cEpswpMCbjzpXm/00HHpUPFLBvVjpAi5hwxsxqGAgKRcyFGOLpps64ljmgZhIOz07tm9PTrClDIgx0XnUA77Ql0MBndFdfC1BTQDuQ3IkpbRFMB7US+MvNFGmkWTg0CSi9s8buWx7xR3CmptnN3/YvdICA4kZ5rWbYxIt2QZpGaPTK4u/rPNQICkfQkAw6L4lbbfzp+K/r7OptCfPTTX3j71CUr8CBgKhkdnFRBKgLKFXbOSugMXOvGybEBTYITJ3/Dj4s5SwJQF0oVAcGJdAwMvEWQfp7YuwXhh/o1x98/tYCVQlEafyMrbayCIoA3Zl2uXMbK7HOc6JM34u4dVzcl+MvFAkLv/GqZvAJAtJxf8/h4A6gICI7Ph+Ggo1bRv3ptOzxdjk2L/2pgtcCVBNiQeVoS/7UKlIzElPBxryxZnf1D927DU/dt04TZ+/K8VRcN4/OFnj5mV9V992kf9tzc3ZTkmQureP5YxnYBKNIIsyP7cGY/v7VTk+AH31zGh99etl0ArwksEEknGHCPFXS/rxvvPePThHjj49/hcXdgdN9WxD6/aL0WlD0R8D0PoUz9BlxUzNj+fozu22JqmF21QMmmoCwLRtJkyrOOUfyFAdxyndsQxtZaUPZmWUC3y4GvJ7frkv9h4R/wny9OWz5FafAjLYAT51V39K6turP/3LEM5i6sGq6OrIGUAKOipZLJ5Yt44PUFWW6mxnEB/BzmJlPWZSOjoqVinTq9hDc/+UMEWsiWCGel0igvWj3uDiWu9aovT59fnrE/7lWV5TQqvonpcTuUtvjBPb149fEbNGftkenz+HNpXWhWRYzLhUz+6OSVx67H/jv6mvq8tLSOR6fPi/ARt+WthJVmTq995gL4OyDyiLYbSjPHHQQm0inGcLuIM7OZyCymaJHjL3DyyKBfekNjtH00S1y1E234ajY0ygWFK5dljF1j1rHe9tEsRrXds+9nkMqYK3hE9HdhzeOtbCk5kGhbrRf/MgJENjzVRys1xyqg9ZTIKsgQtTqGzz5Yp189obP9YMsqQcPxRfbi7PTOmGrXcLQok5EMndpkoGaearimh7sdjmKi3UKpPnQ0V0CpC21wsdGwaGaP19WB7SRC75bm/3vFpK5EcPzcMDHEW/1O8JhnhJClS76NcFr0gq7MiPZLssmHZxsw57At16zVJPgFCIjCm7UapUzDYtUXGEaTYHhPXA/AL0KAKyE7hajEAWfczKzr1gEjxer/lQbQnRsGsbBsaJVChWKFgmemZR97NBOobIquyg2Rg/GvVoYYwD+1qTkoKJEF3yAnWJFS+X89CVnS1Rz+AyYUE1qu2gD/AAAAAElFTkSuQmCC']")
        elem.click()
        scheight = .1
        time.sleep(10)
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        ukrpoolcalcresult = soup.findAll("ul", {"class": "table-header bg"})
        list_of_inner_text = [x.text for x in ukrpoolcalcresult]
        ukrpooltext = ', '.join(list_of_inner_text)
        ukrpool_regex = ukrpooltext.replace(" ", "")[393:-73]
        print(ukrpool_regex)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'ukrpool_ltc.png')
        namepool = "OKKONG/UKRPOOL"
        urlpool = "https://ukrpool.com/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path= '/api/ltc/ukrpool/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': ukrpool_regex, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'ukrpool_ltc.png'))
        except Exception:
            pass


    def handle(self, *args, **kwargs):
        self.btc()
#        self.ltc()
