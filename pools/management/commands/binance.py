from django.core.management.base import BaseCommand
from django.conf import settings
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
import time
import requests
import base64
from decimal import Decimal


class Command(BaseCommand):
    help = 'Parse binance pool calc coin'

    def btc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://pool.binance.com/en/")
        time.sleep(5)
        scheight = .1
        while scheight < 9.9:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'binance.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        binance_btc_calcresult = soup.find("div", {"class": "content-box"})
        list_of_inner_text = [x.text for x in binance_btc_calcresult]
        binance_btc_text = ', '.join(list_of_inner_text)
        binance_btc_regex = binance_btc_text.replace(" ", "")[:-31]
        binance_fee_min = Decimal(binance_btc_regex) * Decimal(1 - 2.5/100)
        binance_fee_min_fix = round(binance_fee_min, 8)
        print(binance_btc_regex)
        print(binance_fee_min_fix)
        driver.quit()
        filename = os.path.join(settings.BASE_DIR, 'binance.png')
        key_imgbb = settings.API_KEY_IMGBB
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        namepool = "BINANCE"
        urlpool = "https://pool.binance.com/en/"
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/btc/binance/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': binance_fee_min_fix, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'binance.png'))
        except Exception:
            pass


    def bch(self):
        print("BCH")

    def handle(self, *args, **kwargs):
        self.btc()
