from django.core.management.base import BaseCommand
from django.conf import settings
import requests
from decimal import Decimal


class Command(BaseCommand):
    help = 'Parse prohashing pool calc coin'

    def btc(self):
        url = requests.get('https://prohashing.com/api/v1/status')
        probtc = url.json()["data"]["sha-256"]["estimate_last24h"]
        probtc_regex = probtc.replace("0.0000000000", "0.0000")[:-2]
        probtc_fee_min = Decimal(probtc_regex) * Decimal(1 - 3.9/100)
        probtc_fee_min_fix = round(probtc_fee_min, 8)
        print(probtc_regex)
        auth_token = settings.POOLS_TOKEN_AUTO
        namepool = "PROHASHING"
        urlpool = "https://prohashing.com/"
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/btc/prohashing/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': probtc_fee_min_fix, "price_check_image": 'https://prohashing.com/api/v1/status'}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)

    def handle(self, *args, **kwargs):
        self.btc()
