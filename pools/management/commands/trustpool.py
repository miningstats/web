from django.core.management.base import BaseCommand
from django.conf import settings
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
import time
import requests
import base64
from decimal import Decimal


class Command(BaseCommand):
    help = 'Parse emcd pool calc coin'

    def btc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://trustpool.ru/")
        time.sleep(5)
        scheight = .1
        while scheight < 5.3:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'trustpool.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        trustcalcresult = soup.find("div", {"class": "m-l-10", "style": "max-width:180px;"})
        trustcalctext = trustcalcresult.text
        trust_regex = trustcalctext.replace(" ", "").replace("TH/s≈", "").replace("BTC", "")
        print(trust_regex)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'trustpool.png')
        namepool = "TRUSTPOOL"
        urlpool = "https://trustpool.ru/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/btc/trustpool/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': trust_regex, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'trustpool.png'))
        except Exception:
            pass


    def ltc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://trustpool.ru/")
        time.sleep(5)
        elem = driver.find_element_by_xpath("//img[@src='/images/coinIcon/pool_btc.svg']")
        elem.click()
        time.sleep(5)
        elem = driver.find_element_by_xpath("//img[@src='/images/coinIcon/pool_ltc.svg']")
        elem.click()
        scheight = .1
        while scheight < 5.3:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'trustpool_ltc.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        trustcalcresult = soup.findAll("div", {"class": "m-l-10", "style": "max-width:180px;"})
        list_of_inner_text = [x.text for x in trustcalcresult]
        trusttext = ', '.join(list_of_inner_text)
        trust_regex = trusttext.replace(" ", "").replace(",", "")[59:-99]
        trust_convert_mh = Decimal(trust_regex) / Decimal(1000)
        trust_round_fix = round(trust_convert_mh, 8)
        # doge + ~15%
        trust_plus_doge = Decimal(trust_round_fix) * Decimal(1 + 15/100)
        trust_plus_doge_round = round(trust_plus_doge, 8)
        print(trust_plus_doge_round)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'trustpool_ltc.png')
        namepool = "TRUSTPOOL"
        urlpool = "https://trustpool.ru/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/ltc/trustpool/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': trust_regex, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'trustpool_ltc.png'))
        except Exception:
            pass


    def bch(self):
        print("BCH")

    def handle(self, *args, **kwargs):
        self.btc()
#        self.ltc()
