from django.core.management.base import BaseCommand
from django.conf import settings
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
import time
import requests
import base64


class Command(BaseCommand):
    help = 'Parse viabtc pool calc coin'

    def btc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://www.viabtc.com/tools/calculator")
        time.sleep(5)
        elem = driver.find_element_by_xpath("//img[@src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAqCAYAAADFw8lbAAADkUlEQVRYR82ZT4hXVRTHP1+CVhotNCWyxEWmqG1ctDCEFuZCV0aMM1GgMgtNowEdY2bhQskSlBwNlEFQaBrEVgWWgiC6aBEuNCxdSEmEYAtRV22OHOf85PnmvXn3/f4M78Ljx4/f797zueece85554oOh5m9CCyNZyEwN5Z8BNwDbvkj6f9ORKmdyWY2H+gDNgLvAHeBP4F/AQf04cCvAm8BrwO/Aj8Ck5Lu15VbC9TMlgFfABtC6DngsqSHMwk2s5eAtcAHsbmfgC8l/ZEKnARqZi8D+4FNwDfAt1VwZQABvR34DPgBGJX0oAq4EtTM1gDfAa6FkZRFq4T677H5A2GdAUlXZ5o3I6iZDQL7gC2Sfk4BqPsfM1sPnHI5kk6WzS8FNbNhYBvwvqQ7dQHq/N/MlgC/AOOSviqaWwgamtwNvCvJQ0zPh5l5aLsCHCrS7DTQ8MmzwJpeazK/+9Cs++qHeZ99DjQc/Dow2CufrDJN+Kz76qrswc2DHgMkaUfVgr383cyOe2CQ9GlLzjPQCOaXgGXdCkHtbiYs68ngvVZSyIKeAW5KOtiugG7OM7O9wHJJH/u6T0Ejd3vxsDgl45jZNeCiJA9hScPMXgDc9xZJWlc1KTLYX17seG3QAt0JrJb0SdUCsTGPdXuAI5KGquYE5GlgABiSdKRqTsjxOb9JGmuBXgDGJHl1kzTM7DDweRVsDnJvWUAvEmpmXp3tdAso6sn/gNdSzJ5dsAq2E8jQqFdd/wDzHHQlMCHJP2uPMtgc5LCkr2svPnV+bgD9Duo1Yp8k/2xr5GG7BRla9Zp30kE9qL4paVdblDEpCwu8EgenbU22WMzsKHDbQb1inyNppBPQ2H3rgPnXWgenTLaZec36uGugOXO73KTQVaWcLGjHps+fbmBBSuiqggwrPTN9R4ep7OBUha4UyPxh8rD0vaQVqZMzju5psZVxph2cbsCa2e/A5rYDfmow7wQ28v1UwA/11kqhqZAZzSel27xFn0uhAVq3KBkHttYJQRnNHpA0muJmZjatKPEWTZ0yzy1wPrUKymjWmxheSn5UBVpY5oVWm184B6j3lZr/KhKwzX+5C1BvhjXhdfkE8Hbp63LAelOs2Q2IzOn05lizWzoZ2OY3yXKabXbbMQPrPjsRrfBeNHL9TbO/o0ZuBrb5rfFsqmv8ZUNBVZO/vvk7aoWi6xu/g3pjVq9vigqJ2boQewL5piMDXeprwAAAAABJRU5ErkJggg==']")
        scheight = .1
        while scheight < 8.0:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'viabtc.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        viabtccalcresult = soup.findAll("span", {"class": "t-main-hg"})
        list_of_inner_text = [x.text for x in viabtccalcresult]
        viabtctext = ', '.join(list_of_inner_text)
        print(viabtctext)
        driver.quit()
        filename = os.path.join(settings.BASE_DIR, 'viabtc.png')
        key_imgbb = settings.API_KEY_IMGBB
        namepool = "VIABTC"
        urlpool = "https://www.viabtc.com/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/btc/viabtc/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': viabtctext, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'viabtc.png'))
        except Exception:
            pass

    def bch(self):
        print("BCH")

    def handle(self, *args, **kwargs):
        self.btc()
