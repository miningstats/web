from django.core.management.base import BaseCommand
from django.conf import settings
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
import time
import requests
import base64


class Command(BaseCommand):
    help = 'Parse emcd pool calc coin'

    def btc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://pool.emcd.io/")
        scheight = .1
        while scheight < 11.9:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'emcd.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        emcdcalcresult = soup.find("div", {"class": "calc-result-value"})
        emcdcalctext = emcdcalcresult.text
        emcdcalcresult_regex = emcdcalctext.replace(" ", "")[:-5]
        print(emcdcalcresult_regex)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'emcd.png')
        namepool = "EMCD"
        urlpool = "https://pool.emcd.io/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/btc/emcd/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': emcdcalcresult_regex, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'emcd.png'))
        except Exception:
            pass


    def ltc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(60)
        driver.get("https://pool.emcd.io/")
        time.sleep(15)
        elem = driver.find_element_by_class_name("el-dropdown-selfdefine")
        elem.click()
        time.sleep(15)
        elem = driver.find_element_by_xpath("//span[text()='Litecoin ']")
        elem.click()
        time.sleep(5)
        elem = driver.find_element_by_xpath(".//jdiv[@id='jivo_close_button']")
        elem.click()
        scheight = .1
        while scheight < 11.9:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'emcd_ltc.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        emcdcalcresult = soup.find("div", {"class": "calc-result-value"})
        emcdcalctext = emcdcalcresult.text
        emcdcalcresult_regex = emcdcalctext.replace(" ", "")[:-5]
        print(emcdcalcresult_regex)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'emcd_ltc.png')
        namepool = "EMCD"
        urlpool = "https://pool.emcd.io/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/ltc/emcd/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': emcdcalcresult_regex, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'emcd_ltc.png'))
        except Exception:
            pass


    def eth(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(60)
        driver.get("https://pool.emcd.io/")
        time.sleep(15)
        elem = driver.find_element_by_class_name("el-dropdown-selfdefine")
        elem.click()
        time.sleep(15)
        elem = driver.find_element_by_xpath("//span[text()='Ethereum ']")
        elem.click()
        time.sleep(5)
        elem = driver.find_element_by_xpath(".//jdiv[@id='jivo_close_button']")
        elem.click()
        scheight = .1
        while scheight < 11.9:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'emcd_eth.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        emcdcalcresult = soup.find("div", {"class": "calc-result-value"})
        emcdcalctext = emcdcalcresult.text
        emcdcalcresult_regex = emcdcalctext.replace(" ", "")[:-5]
        print(emcdcalcresult_regex)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'emcd_eth.png')
        namepool = "EMCD"
        urlpool = "https://pool.emcd.io/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/eth/emcd/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': emcdcalcresult_regex, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'emcd_eth.png'))
        except Exception:
            pass

    def handle(self, *args, **kwargs):
        self.btc()
#        self.ltc()
#        self.eth()
