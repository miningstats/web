from django.core.management.base import BaseCommand
from django.conf import settings
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import requests
import os
import base64
from pathlib import Path


class Command(BaseCommand):
    help = 'Parse viabtc pool calc coin'

    def btc_boost(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://expool.org/")
        scheight = .1
        while scheight < 9.9:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'expool_boost.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        expoolboostcalcresult = soup.findAll("h1", {"class": "card-title pricing-card-title"})
        list_of_inner_text = [x.text for x in expoolboostcalcresult]
        expoolboosttext = ''.join(list_of_inner_text)
        expoolboost_regex = expoolboosttext.replace(" ", "")[13:][:-49]
        print(expoolboost_regex)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'expool_boost.png')
        namepool = "EXPOOLAB"
        urlpool = "https://expool.org/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/btc/expool_ab/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': expoolboost_regex, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'expool_boost.png'))
        except Exception:
            pass

    def btc_no_boost(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://expool.org/")
        scheight = .1
        while scheight < 9.9:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'expool_no_boost.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        expoolno_boostcalcresult = soup.findAll("h1", {"class": "card-title pricing-card-title"})
        list_of_inner_text = [x.text for x in expoolno_boostcalcresult]
        expoolno_boosttext = ''.join(list_of_inner_text)
        expoolno_boost_regex = expoolno_boosttext.replace(" ", "")[1:][:-61]
        print(expoolno_boost_regex)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'expool_no_boost.png')
        namepool = "EXPOOLNOAB"
        urlpool = "https://expool.org/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/btc/expool_no_ab/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': expoolno_boost_regex, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)

    def bch(self):
        print("BCH")

    def handle(self, *args, **kwargs):
        self.btc_boost()
        self.btc_no_boost()
