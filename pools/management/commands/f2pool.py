from django.core.management.base import BaseCommand
from django.conf import settings
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import os
import time
import requests
import base64


class Command(BaseCommand):
    help = 'Parse viabtc pool calc coin'

    def btc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://www.f2pool.com/")
        time.sleep(5)
#        modal = driver.find_element_by_link_text("Calc")
#        modal.click()
        elem = driver.find_element_by_xpath("//img[@src='https://static.f2pool.com/static/images/currency-btc.png?v=be599586696902a5cb5ef9c97023df55']")
        elem.click()
        scheight = .1
        while scheight < 9.9:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'f2pool.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        f2calcresult = soup.find("span", {"class": "pl-1 profit-val info-value"}).text
        f2_btc_regex = f2calcresult.replace(" ", "").replace(",", "")[3:][:-2]
        print(f2_btc_regex)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'f2pool.png')
        namepool = "F2POOL"
        urlpool = "https://www.f2pool.com/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/btc/f2pool/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': f2_btc_regex, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'f2pool.png'))
        except Exception:
            pass

    def ltc(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://www.f2pool.com/")
        time.sleep(5)
        elem = driver.find_element_by_xpath("//img[@src='https://static.f2pool.com/static/images/currency-ltc.png?v=c55a04c751487761d3b52db4753edfc8']")
        elem.click()
        scheight = .1
        while scheight < 9.9:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'f2pool_ltc.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        f2calcresult = soup.find("span", {"class": "pl-1 profit-val info-value"}).text
        f2_btc_regex = f2calcresult.replace(" ", "").replace(",", "")[3:][:-2]
        print(f2_btc_regex)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'f2pool_ltc.png')
        namepool = "F2POOL"
        urlpool = "https://www.f2pool.com/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/ltc/f2pool/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': f2_btc_regex, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'f2pool_ltc.png'))
        except Exception:
            pass

    def eth(self):
        binary = settings.SELENIUM_FIREFOX_BINARY
        options = Options()
        options.headless = settings.SELENIUM_HEADLESS
        options.binary = binary
        cap = DesiredCapabilities().FIREFOX.copy()
        cap["marionette"] = True
        driver = webdriver.Firefox(firefox_options=options, capabilities=cap, executable_path=settings.SELENIUM_GECKODRIVER_BINARY)
        driver.implicitly_wait(30)
        driver.get("https://www.f2pool.com/")
        time.sleep(5)
        elem = driver.find_element_by_xpath("//img[@src='https://static.f2pool.com/static/images/currency-eth.png?v=cb49ca75c8497e9966c3dd50daae2f13']")
        elem.click()
        scheight = .1
        while scheight < 9.9:
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight/%s);" % scheight)
            scheight += .01
        driver.save_screenshot(os.path.join(settings.BASE_DIR, 'f2pool_eth.png'))
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        f2calcresult = soup.find("span", {"class": "pl-1 profit-val info-value"}).text
        f2_btc_regex = f2calcresult.replace(" ", "").replace(",", "")[3:][:-2]
        print(f2_btc_regex)
        driver.quit()
        key_imgbb = settings.API_KEY_IMGBB
        filename = os.path.join(settings.BASE_DIR, 'f2pool_eth.png')
        namepool = "F2POOL"
        urlpool = "https://www.f2pool.com/"
        with open(filename, "rb") as file:
            url = "https://api.imgbb.com/1/upload"
            payload = {
                "key": key_imgbb,
                "image": base64.b64encode(file.read()),
            }
            res = requests.post(url, payload)
            imgbburl = res.json()["data"]["url"]
        imgbburl = imgbburl
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/eth/f2pool/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': f2_btc_regex, "price_check_image": imgbburl}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)
        try:
            os.remove(os.path.join(settings.BASE_DIR, 'f2pool_eth.png'))
        except Exception:
            pass

    def bch(self):
        print("BCH")

    def handle(self, *args, **kwargs):
        self.btc()
#        self.ltc()
#        self.eth()
