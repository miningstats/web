from django.apps import AppConfig


class PoolsConfig(AppConfig):
    name = 'pools'
    verbose_name = 'Статистика пулов sha256'
