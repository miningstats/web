from django.contrib import admin
from .models import Binance, BTC, EMCD, EXPOOL, EXPOOLAB,\
    F2, POOLIN, PROHASHING, RUPOOLPRO, SIGMAPOOL, TRUSTPOOL,\
    UKRPOOL, VIABTC, btcmachinelearning


class BinanceAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class BTCAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class EMCDAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class EXPOOLAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class EXPOOLABAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class F2Admin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class POOLINAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class PROHASHINGAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class RUPOOLPROAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class SIGMAPOOLAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class TRUSTPOOLAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class UKRPOOLAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class VIABTCAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class btcmachinelearningAdmin(admin.ModelAdmin):
    list_display = ('url',)
    list_display_links = ('url',)


admin.site.register(Binance, BinanceAdmin)
admin.site.register(BTC, BTCAdmin)
admin.site.register(EMCD, EMCDAdmin)
admin.site.register(EXPOOL, EXPOOLAdmin)
admin.site.register(EXPOOLAB, EXPOOLABAdmin)
admin.site.register(F2, F2Admin)
admin.site.register(POOLIN, POOLINAdmin)
admin.site.register(PROHASHING, PROHASHINGAdmin)
admin.site.register(RUPOOLPRO, RUPOOLPROAdmin)
admin.site.register(SIGMAPOOL, SIGMAPOOLAdmin)
admin.site.register(TRUSTPOOL, TRUSTPOOLAdmin)
admin.site.register(UKRPOOL, UKRPOOLAdmin)
admin.site.register(VIABTC, VIABTCAdmin)
admin.site.register(btcmachinelearning, btcmachinelearningAdmin)
