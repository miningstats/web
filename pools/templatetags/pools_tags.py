from django import template
from pools.models import Binance, BTC, EMCD, EXPOOL, F2, POOLIN, PROHASHING,\
    RUPOOLPRO, SIGMAPOOL, TRUSTPOOL, UKRPOOL, VIABTC

register = template.Library()


@register.simple_tag
def binance_review(product_id):
    product = Binance.objects.get(id=product_id)