from itertools import chain
from operator import attrgetter
from django.views.generic import ListView, DetailView
from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from .models import Binance, BTC, EMCD, EXPOOL, EXPOOLAB, F2, POOLIN, PROHASHING, RUPOOLPRO,\
    SIGMAPOOL, TRUSTPOOL, UKRPOOL, VIABTC, btcmachinelearning

from .serializers import BinanceSerializer, BinanceSerializerPub, BTCSerializer, BTCSerializerPub,\
    EMCDSerializer, EMCDSerializerPub, EXPOOLSerializer, EXPOOLSerializerPub, EXPOOLABSerializer,\
    EXPOOLABSerializerPub, F2Serializer, F2SerializerPub, POOLINSerializer, POOLINSerializerPub,\
    PROHASHINGSerializer, PROHASHINGSerializerPub, RUPOOLPROSerializer, RUPOOLPROSerializerPub,\
    SIGMAPOOLSerializer, SIGMAPOOLSerializerPub, TRUSTPOOLSerializer, TRUSTPOOLSerializerPub,\
    UKRPOOLSerializer, UKRPOOLSerializerPub, VIABTCSerializer, VIABTCSerializerPub,\
    btcmachinelearningSerializerPub, btcmachinelearningSerializer


class index(ListView):
    template_name = 'pool_list_btc.html'
    context_object_name = 'poolslist'


    def get_queryset(self):
        qs1 = Binance.objects.all().order_by('-published')[:1]
        qs2 = BTC.objects.all().order_by('-published')[:1]
        qs3 = EMCD.objects.all().order_by('-published')[:1]
        qs4 = EXPOOL.objects.all().order_by('-published')[:1]
        qs5 = EXPOOLAB.objects.all().order_by('-published')[:1]
        qs6 = F2.objects.all().order_by('-published')[:1]
        qs7 = POOLIN.objects.all().order_by('-published')[:1]
        qs8 = PROHASHING.objects.all().order_by('-published')[:1]
        qs9 = RUPOOLPRO.objects.all().order_by('-published')[:1]
        qs10 = SIGMAPOOL.objects.all().order_by('-published')[:1]
        qs11 = TRUSTPOOL.objects.all().order_by('-published')[:1]
        qs12 = UKRPOOL.objects.all().order_by('-published')[:1]
        qs13 = VIABTC.objects.all().order_by('-published')[:1]
        queryset = sorted(chain(
            qs1, qs2, qs3, qs4, qs5, qs6, qs7, qs8,\
            qs9, qs10, qs11, qs12, qs13),
                          key=attrgetter('price'), reverse=True)
        return queryset

class rupool_detali(ListView):
    template_name = 'rupoolpro.html'
    context_object_name = 'rupoolpro'

    def get_queryset(self):
        qs1 = RUPOOLPRO.objects.all().order_by('-published')[:30]
        queryset = sorted(chain(qs1),
                          key=attrgetter('published'), reverse=True)
        return queryset


class binance_detali(ListView):
    template_name = 'binance.html'
    context_object_name = 'binance'

    def get_queryset(self):
        qs1 = Binance.objects.all().order_by('-published')[:30]
        queryset = sorted(chain(qs1),
                          key=attrgetter('published'), reverse=True)
        return queryset

class btccom_detali(ListView):
    template_name = 'btccom.html'
    context_object_name = 'btccom'

    def get_queryset(self):
        qs1 = BTC.objects.all().order_by('-published')[:30]
        queryset = sorted(chain(qs1),
                          key=attrgetter('published'), reverse=True)
        return queryset

class emcd_detali(ListView):
    template_name = 'emcd.html'
    context_object_name = 'emcd'

    def get_queryset(self):
        qs1 = EMCD.objects.all().order_by('-published')[:30]
        queryset = sorted(chain(qs1),
                          key=attrgetter('published'), reverse=True)
        return queryset

class f2_detali(ListView):
    template_name = 'f2pool.html'
    context_object_name = 'f2pool'

    def get_queryset(self):
        qs1 = F2.objects.all().order_by('-published')[:30]
        queryset = sorted(chain(qs1),
                          key=attrgetter('published'), reverse=True)
        return queryset


class poolin_detali(ListView):
    template_name = 'poolin.html'
    context_object_name = 'poolin'

    def get_queryset(self):
        qs1 = POOLIN.objects.all().order_by('-published')[:30]
        queryset = sorted(chain(qs1),
                          key=attrgetter('published'), reverse=True)
        return queryset

class prohashing_detali(ListView):
    template_name = 'prohashing.html'
    context_object_name = 'prohashing'

    def get_queryset(self):
        qs1 = PROHASHING.objects.all().order_by('-published')[:30]
        queryset = sorted(chain(qs1),
                          key=attrgetter('published'), reverse=True)
        return queryset


class sigmapool_detali(ListView):
    template_name = 'sigmapool.html'
    context_object_name = 'sigmapool'

    def get_queryset(self):
        qs1 = SIGMAPOOL.objects.all().order_by('-published')[:30]
        queryset = sorted(chain(qs1),
                          key=attrgetter('published'), reverse=True)
        return queryset


class trustpool_detali(ListView):
    template_name = 'trustpool.html'
    context_object_name = 'trustpool'

    def get_queryset(self):
        qs1 = TRUSTPOOL.objects.all().order_by('-published')[:30]
        queryset = sorted(chain(qs1),
                          key=attrgetter('published'), reverse=True)
        return queryset


class ukrpool_detali(ListView):
    template_name = 'ukrpool.html'
    context_object_name = 'ukrpool'

    def get_queryset(self):
        qs1 = UKRPOOL.objects.all().order_by('-published')[:30]
        queryset = sorted(chain(qs1),
                          key=attrgetter('published'), reverse=True)
        return queryset


class viabtc_detali(ListView):
    template_name = 'viabtc.html'
    context_object_name = 'viabtc'

    def get_queryset(self):
        qs1 = VIABTC.objects.all().order_by('-published')[:30]
        queryset = sorted(chain(qs1),
                          key=attrgetter('published'), reverse=True)
        return queryset

class api_all_pool_view_set(viewsets.ModelViewSet):
    serializer_class = (BinanceSerializerPub)
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(request):
        qs1 = Binance.objects.all().order_by('-published')[:1]
        qs2 = BTC.objects.all().order_by('-published')[:1]
        qs3 = EMCD.objects.all().order_by('-published')[:1]
        qs4 = EXPOOL.objects.all().order_by('-published')[:1]
        qs5 = EXPOOLAB.objects.all().order_by('-published')[:1]
        qs6 = F2.objects.all().order_by('-published')[:1]
        qs7 = POOLIN.objects.all().order_by('-published')[:1]
        qs8 = PROHASHING.objects.all().order_by('-published')[:1]
        qs9 = RUPOOLPRO.objects.all().order_by('-published')[:1]
        qs10 = SIGMAPOOL.objects.all().order_by('-published')[:1]
        qs11 = TRUSTPOOL.objects.all().order_by('-published')[:1]
        qs12 = UKRPOOL.objects.all().order_by('-published')[:1]
        qs13 = VIABTC.objects.all().order_by('-published')[:1]
        queryset = sorted(chain(
            qs1, qs2, qs3, qs4, qs5, qs6, qs7, qs8,\
            qs9, qs10, qs11, qs12, qs13),
                          key=attrgetter('price'), reverse=True)
        return queryset


class api_binance_view_set(viewsets.ModelViewSet):
    queryset = Binance.objects.all().order_by('-published')
    serializer_class = BinanceSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_binance_pub(viewsets.ModelViewSet):
    model = Binance.objects.all().order_by('-published')
    serializer_class = BinanceSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return Binance.objects.all().order_by('-published')


class api_btc_view_set(viewsets.ModelViewSet):
    queryset = BTC.objects.all().order_by('-published')
    serializer_class = BTCSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_btc_pub(viewsets.ModelViewSet):
    model = BTC.objects.all().order_by('-published')
    serializer_class = BTCSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return BTC.objects.all().order_by('-published')


class api_emcd_view_set(viewsets.ModelViewSet):
    queryset = EMCD.objects.all().order_by('-published')
    serializer_class = EMCDSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_emcd_pub(viewsets.ModelViewSet):
    model = EMCD.objects.all().order_by('-published')
    serializer_class = EMCDSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return EMCD.objects.all().order_by('-published')


class api_expool_noab_view_set(viewsets.ModelViewSet):
    queryset = EXPOOL.objects.all().order_by('-published')
    serializer_class = EXPOOLSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_expool_noab_pub(viewsets.ModelViewSet):
    model = EXPOOL.objects.all().order_by('-published')
    serializer_class = EXPOOLSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return EXPOOL.objects.all().order_by('-published')


class api_expool_ab_view_set(viewsets.ModelViewSet):
    queryset = EXPOOLAB.objects.all().order_by('-published')
    serializer_class = EXPOOLABSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_expool_ab_pub(viewsets.ModelViewSet):
    model = EXPOOLAB.objects.all().order_by('-published')
    serializer_class = EXPOOLABSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return EXPOOLAB.objects.all().order_by('-published')


class api_f2_view_set(viewsets.ModelViewSet):
    queryset = F2.objects.all().order_by('-published')
    serializer_class = F2Serializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_f2_pub(viewsets.ModelViewSet):
    model = F2.objects.all().order_by('-published')
    serializer_class = F2SerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return F2.objects.all().order_by('-published')


class api_poolin_view_set(viewsets.ModelViewSet):
    queryset = POOLIN.objects.all().order_by('-published')
    serializer_class = POOLINSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_poolin_pub(viewsets.ModelViewSet):
    model = POOLIN.objects.all().order_by('-published')
    serializer_class = POOLINSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return POOLIN.objects.all().order_by('-published')


class api_prohashing_view_set(viewsets.ModelViewSet):
    queryset = PROHASHING.objects.all().order_by('-published')
    serializer_class = PROHASHINGSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_prohashing_pub(viewsets.ModelViewSet):
    model = PROHASHING.objects.all().order_by('-published')
    serializer_class = PROHASHINGSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return PROHASHING.objects.all().order_by('-published')


class api_rupoolpro_view_set(viewsets.ModelViewSet):
    queryset = RUPOOLPRO.objects.all().order_by('-published')
    serializer_class = RUPOOLPROSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_rupoolpro_pub(viewsets.ModelViewSet):
    model = RUPOOLPRO.objects.all().order_by('-published')
    serializer_class = RUPOOLPROSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return RUPOOLPRO.objects.all().order_by('-published')


class api_sigmapool_view_set(viewsets.ModelViewSet):
    queryset = SIGMAPOOL.objects.all().order_by('-published')
    serializer_class = SIGMAPOOLSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_sigmapool_pub(viewsets.ModelViewSet):
    model = SIGMAPOOL.objects.all().order_by('-published')
    serializer_class = SIGMAPOOLSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return SIGMAPOOL.objects.all().order_by('-published')


class api_trustpool_view_set(viewsets.ModelViewSet):
    queryset = TRUSTPOOL.objects.all().order_by('-published')
    serializer_class = TRUSTPOOLSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_trustpool_pub(viewsets.ModelViewSet):
    model = TRUSTPOOL.objects.all().order_by('-published')
    serializer_class = TRUSTPOOLSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return TRUSTPOOL.objects.all().order_by('-published')


class api_ukrpool_view_set(viewsets.ModelViewSet):
    queryset = UKRPOOL.objects.all().order_by('-published')
    serializer_class = UKRPOOLSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_ukrpool_pub(viewsets.ModelViewSet):
    model = UKRPOOL.objects.all().order_by('-published')
    serializer_class = UKRPOOLSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return UKRPOOL.objects.all().order_by('-published')


class api_viabtc_view_set(viewsets.ModelViewSet):
    queryset = VIABTC.objects.all().order_by('-published')
    serializer_class = VIABTCSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_viabtc_pub(viewsets.ModelViewSet):
    model = VIABTC.objects.all().order_by('-published')
    serializer_class = VIABTCSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return VIABTC.objects.all().order_by('-published')


class api_btcmachinelearning_pub(viewsets.ModelViewSet):
    model = btcmachinelearning.objects.all()
    serializer_class = btcmachinelearningSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return btcmachinelearning.objects.all()


class api_btcmachinelearning_view_set(viewsets.ModelViewSet):
    queryset = btcmachinelearning.objects.all()
    serializer_class = btcmachinelearningSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)
