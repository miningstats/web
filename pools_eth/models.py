from django.db import models

class Minerall(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    reward = models.DecimalField(
        max_digits=27,
        decimal_places=24,
    )
    hashrate = models.DecimalField(
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'MINERALL'
        verbose_name = 'MINERALL'
        ordering = ['-price', '-published']

class BTC(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'BTC_com ETH'
        verbose_name = 'BTC_com ETH'
        ordering = ['-price', '-published']

class EMCD(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'EMCD ETH'
        verbose_name = 'EMCD ETH'
        ordering = ['-price', '-published']

class F2(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'F2 ETH'
        verbose_name = 'F2 ETH'
        ordering = ['-price', '-published']

class POOLIN(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'POOLIN ETH'
        verbose_name = 'POOLIN ETH'
        ordering = ['-price', '-published']


class TRUSTPOOL(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'TRUSTPOOL ETH'
        verbose_name = 'TRUSTPOOL ETH'
        ordering = ['-price', '-published']


class VIABTC(models.Model):
    name = models.CharField(
        max_length=9999,
    )
    url = models.CharField(
        max_length=99999
    )
    price = models.DecimalField(
        db_index=True,
        max_digits=19,
        decimal_places=8,
    )
    published = models.DateTimeField(
        auto_now_add=True,
        db_index=True
    )
    price_check_image = models.CharField(
        blank=True,
        null=True,
        max_length=9999
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'VIABTC ETH'
        verbose_name = 'VIABTC ETH'
        ordering = ['-price', '-published']
