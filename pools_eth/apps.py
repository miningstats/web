from django.apps import AppConfig


class PoolsEthConfig(AppConfig):
    name = 'pools_eth'
