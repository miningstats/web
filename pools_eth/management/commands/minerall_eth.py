# pplns profits calculated formula
# https://minerall.io/luck
#            Hashrate / Pool Hashrate * Reward * (1 - Pool Fee)
# 2020-10-15   1           1.206       90.10228     2%
# Decimal(1) / Decimal(1206000) *  Decimal(90.10228) * Decimal(1 - 2/100)
# ~ 0.00007321
from django.core.management.base import BaseCommand
from django.conf import settings
import requests
from decimal import Decimal


class Command(BaseCommand):
    help = 'Parse minerall pool calc coin'

    def eth(self):
        url = requests.get('https://user.minerall.io/api/statistics/luck-period/?coin=1&format=json')
        reward = url.json()[1]["amount"]
        hashrate = url.json()[1]["hashrate"] / Decimal(10**6)
        minerall_eth_calculated = Decimal(1) / Decimal(hashrate) *  Decimal(reward) * Decimal(1 - 2/100)
        minerall_eth_calculated = round(minerall_eth_calculated, 8)
        print(minerall_eth_calculated)
        namepool = "MINERALL"
        urlpool = "https://minerall.io/"
        auth_token = settings.POOLS_TOKEN_AUTO
        hed = {'Authorization': 'Token ' + auth_token}
        url_api_path = '/api/eth/minerall/post/?format=json'
        url = settings.SITE_URL + url_api_path
        payload = {'name': namepool, 'url': urlpool, 'price': minerall_eth_calculated, "price_check_image": 'https://minerall.io/luck', 'reward': reward, 'hashrate': hashrate}
        r = requests.get(url, headers=hed)
        r = requests.get(url, params=payload, headers=hed)
        r = requests.post(url, data=payload, headers=hed)


    def handle(self, *args, **kwargs):
        self.eth()
