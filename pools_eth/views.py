from itertools import chain
from operator import attrgetter
from django.views.generic import ListView
from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny
from .models import BTC, EMCD, F2, POOLIN, TRUSTPOOL, VIABTC, Minerall

from .serializers import BTCSerializer, BTCSerializerPub, EMCDSerializer,\
    EMCDSerializerPub, F2Serializer, F2SerializerPub, POOLINSerializer,\
    POOLINSerializerPub, TRUSTPOOLSerializer, TRUSTPOOLSerializerPub,\
    VIABTCSerializer, VIABTCSerializerPub, MinerallSerializer,\
    MinerallSerializerPub


class index(ListView):
    template_name = 'pool_list_eth.html'
    context_object_name = 'poolslist_eth'


    def get_queryset(self):
        qs1 = BTC.objects.all().order_by('-published')[:1]
        qs2 = EMCD.objects.all().order_by('-published')[:1]
        qs3 = F2.objects.all().order_by('-published')[:1]
        qs4 = POOLIN.objects.all().order_by('-published')[:1]
        qs5 = TRUSTPOOL.objects.all().order_by('-published')[:1]
        qs6 = VIABTC.objects.all().order_by('-published')[:1]
        qs7 = Minerall.objects.all().order_by('-published')[:1]
        queryset = sorted(chain(
            qs1, qs2, qs3, qs4, qs5, qs6, qs7),
                          key=attrgetter('price'), reverse=True)
        return queryset


class api_eth_all_pool_view_set(viewsets.ModelViewSet):
    serializer_class = (BTCSerializerPub)
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        qs1 = BTC.objects.all().order_by('-published')[:1]
        qs2 = EMCD.objects.all().order_by('-published')[:1]
        qs3 = F2.objects.all().order_by('-published')[:1]
        qs4 = POOLIN.objects.all().order_by('-published')[:1]
        qs5 = TRUSTPOOL.objects.all().order_by('-published')[:1]
        qs6 = VIABTC.objects.all().order_by('-published')[:1]
        qs7 = Minerall.objects.all().order_by('-published')[:1]
        queryset = sorted(chain(
            qs1, qs2, qs3, qs4, qs5, qs6, qs7),
                          key=attrgetter('price'), reverse=True)
        return queryset


class api_btc_eth_view_set(viewsets.ModelViewSet):
    queryset = BTC.objects.all().order_by('-published')
    serializer_class = BTCSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_btc_eth_pub(viewsets.ModelViewSet):
    model = BTC.objects.all().order_by('-published')
    serializer_class = BTCSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return BTC.objects.all().order_by('-published')


class api_emcd_eth_view_set(viewsets.ModelViewSet):
    queryset = EMCD.objects.all().order_by('-published')
    serializer_class = EMCDSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_emcd_eth_pub(viewsets.ModelViewSet):
    model = EMCD.objects.all().order_by('-published')
    serializer_class = EMCDSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return EMCD.objects.all().order_by('-published')


class api_f2_eth_view_set(viewsets.ModelViewSet):
    queryset = F2.objects.all().order_by('-published')
    serializer_class = F2Serializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_f2_eth_pub(viewsets.ModelViewSet):
    model = F2.objects.all().order_by('-published')
    serializer_class = F2SerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return F2.objects.all().order_by('-published')


class api_poolin_eth_view_set(viewsets.ModelViewSet):
    queryset = POOLIN.objects.all().order_by('-published')
    serializer_class = POOLINSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_poolin_eth_pub(viewsets.ModelViewSet):
    model = POOLIN.objects.all().order_by('-published')
    serializer_class = POOLINSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return POOLIN.objects.all().order_by('-published')


class api_trustpool_eth_view_set(viewsets.ModelViewSet):
    queryset = TRUSTPOOL.objects.all().order_by('-published')
    serializer_class = TRUSTPOOLSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_trustpool_eth_pub(viewsets.ModelViewSet):
    model = TRUSTPOOL.objects.all().order_by('-published')
    serializer_class = TRUSTPOOLSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return TRUSTPOOL.objects.all().order_by('-published')


class api_viabtc_eth_view_set(viewsets.ModelViewSet):
    queryset = VIABTC.objects.all().order_by('-published')
    serializer_class = VIABTCSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_viabtc_eth_pub(viewsets.ModelViewSet):
    model = VIABTC.objects.all().order_by('-published')
    serializer_class = VIABTCSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return VIABTC.objects.all().order_by('-published')


class api_minerall_eth_view_set(viewsets.ModelViewSet):
    queryset = Minerall.objects.all().order_by('-published')
    serializer_class = MinerallSerializer
    permission_classes = (IsAdminUser, IsAuthenticated)


class api_minerall_eth_pub(viewsets.ModelViewSet):
    model = VIABTC.objects.all().order_by('-published')
    serializer_class = MinerallSerializerPub
    permission_classes = (AllowAny,)
    http_method_names = ['get', 'head']

    def get_queryset(self):
        return Minerall.objects.all().order_by('-published')
