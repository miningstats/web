from rest_framework import serializers
from .models import Minerall, BTC, EMCD, F2, POOLIN, TRUSTPOOL, VIABTC


class MinerallSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Minerall
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'reward', 'hashrate')


class MinerallSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = Minerall
        fields = ('name', 'url', 'price', 'published', 'price_check_image', 'reward', 'hashrate')


class BTCSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BTC
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class BTCSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = BTC
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class EMCDSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EMCD
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class EMCDSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = EMCD
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class F2Serializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = F2
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class F2SerializerPub(serializers.ModelSerializer):
    class Meta:
        model = F2
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class POOLINSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = POOLIN
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class POOLINSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = POOLIN
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class TRUSTPOOLSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TRUSTPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class TRUSTPOOLSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = TRUSTPOOL
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class VIABTCSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = VIABTC
        fields = ('name', 'url', 'price', 'published', 'price_check_image')


class VIABTCSerializerPub(serializers.ModelSerializer):
    class Meta:
        model = VIABTC
        fields = ('name', 'url', 'price', 'published', 'price_check_image')
