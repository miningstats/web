from django.contrib import admin
from .models import BTC, EMCD, F2, POOLIN, TRUSTPOOL, VIABTC, Minerall


class MinerallAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image', 'reward', 'hashrate')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']

class BTCAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class EMCDAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class F2Admin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class POOLINAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class TRUSTPOOLAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


class VIABTCAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'price', 'published', 'price_check_image')
    list_display_links = ('name',)
    search_fields = ('price',)
    list_editable = ('price',)
    ordering = ['-published']


admin.site.register(Minerall, MinerallAdmin)
admin.site.register(BTC, BTCAdmin)
admin.site.register(EMCD, EMCDAdmin)
admin.site.register(F2, F2Admin)
admin.site.register(POOLIN, POOLINAdmin)
admin.site.register(TRUSTPOOL, TRUSTPOOLAdmin)
admin.site.register(VIABTC, VIABTCAdmin)
