from django.contrib import admin
from django.urls import path, include
from pools import views
from rest_framework import routers
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import ListView, TemplateView

api_info = openapi.Info(
   title="Mining Stats api doc",
   default_version='v1',
)

schema_view = get_schema_view(
   openapi.Info(
      title="Mining Stats api doc",
      default_version='v1',
      description="CRYPTO SCAM",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

router = routers.DefaultRouter()
router.register(r'btc/all/get', views.api_all_pool_view_set, basename='all_pool_get')
router.register(r'btc/binance/post', views.api_binance_view_set, basename='banan post')
router.register(r'btc/binance/get', views.api_binance_pub, basename='banan')
router.register(r'btc/btccom/post', views.api_btc_view_set, basename='btccom post')
router.register(r'btc/btccom/get', views.api_btc_pub, basename='btccom')
router.register(r'btc/emcd/post', views.api_emcd_view_set, basename='emcd post')
router.register(r'btc/emcd/get', views.api_emcd_pub, basename='emcd')
router.register(r'btc/expool_no_ab/post', views.api_expool_noab_view_set, basename='expoolnoab post')
router.register(r'btc/expool_no_ab/get', views.api_expool_noab_pub, basename='expoolnoab')
router.register(r'btc/expool_ab/post', views.api_expool_ab_view_set, basename='expoolab post')
router.register(r'btc/expool_ab/get', views.api_expool_ab_pub, basename='expoolab')
router.register(r'btc/f2pool/post', views.api_f2_view_set, basename='f2pool post')
router.register(r'btc/f2pool/get', views.api_f2_pub, basename='f2pool')
router.register(r'btc/poolin/post', views.api_poolin_view_set, basename='poolin post')
router.register(r'btc/poolin/get', views.api_poolin_pub, basename='poolin')
router.register(r'btc/prohashing/post', views.api_prohashing_view_set, basename='prohashing post')
router.register(r'btc/prohashing/get', views.api_prohashing_pub, basename='prohashing')
router.register(r'btc/rupoolpro/post', views.api_rupoolpro_view_set, basename='rupoolpro post')
router.register(r'btc/rupoolpro/get', views.api_rupoolpro_pub, basename='rupoolpro')
router.register(r'btc/sigmapool/post', views.api_sigmapool_view_set, basename='sigmapool post')
router.register(r'btc/sigmapool/get', views.api_sigmapool_pub, basename='sigmapool')
router.register(r'btc/trustpool/post', views.api_trustpool_view_set, basename='trustpool post')
router.register(r'btc/trustpool/get', views.api_trustpool_pub, basename='trustpool')
router.register(r'btc/ukrpool/post', views.api_ukrpool_view_set, basename='ukrpool post')
router.register(r'btc/ukrpool/get', views.api_ukrpool_pub, basename='ukrpool')
router.register(r'btc/viabtc/post', views.api_viabtc_view_set, basename='viabtc post')
router.register(r'btc/viabtc/get', views.api_viabtc_pub, basename='viabtc')

from pools_eth import views
# ETH
router.register(r'eth/all/get', views.api_eth_all_pool_view_set, basename='all_pool_eth_get')
router.register(r'eth/btccom/post', views.api_btc_eth_view_set, basename='btccom_eth post')
router.register(r'eth/btccom/get', views.api_btc_eth_pub, basename='btccom_eth')
router.register(r'eth/emcd/post', views.api_emcd_eth_view_set, basename='emcd_eth post')
router.register(r'eth/emcd/get', views.api_emcd_eth_pub, basename='emcd_eth')
router.register(r'eth/f2pool/post', views.api_f2_eth_view_set, basename='f2pool_eth post')
router.register(r'eth/f2pool/get', views.api_f2_eth_pub, basename='f2pool_eth')
router.register(r'eth/poolin/post', views.api_poolin_eth_view_set, basename='poolin_eth post')
router.register(r'eth/poolin/get', views.api_poolin_eth_pub, basename='poolin_eth')
router.register(r'eth/trustpool/post', views.api_trustpool_eth_view_set, basename='trustpool_eth post')
router.register(r'eth/trustpool/get', views.api_trustpool_eth_pub, basename='trustpool_eth')
router.register(r'eth/viabtc/post', views.api_viabtc_eth_view_set, basename='viabtc_eth post')
router.register(r'eth/viabtc/get', views.api_viabtc_eth_pub, basename='viabtc_eth')
router.register(r'eth/minerall/post', views.api_minerall_eth_view_set, basename='minerall_eth post')
router.register(r'eth/minerall/get', views.api_minerall_eth_pub, basename='minerall_eth')

from pools_ltc import views
# LTC
router.register(r'ltc/all/get', views.api_ltc_all_pool_view_set, basename='all_pool_ltc_get')
router.register(r'ltc/btccom/post', views.api_btc_ltc_view_set, basename='btccom_ltc post')
router.register(r'ltc/btccom/get', views.api_btc_ltc_pub, basename='btccom_ltc')
router.register(r'ltc/emcd/post', views.api_emcd_ltc_view_set, basename='emcd_ltc post')
router.register(r'ltc/emcd/get', views.api_emcd_ltc_pub, basename='emcd_ltc')
router.register(r'ltc/f2pool/post', views.api_f2_ltc_view_set, basename='f2pool_ltc post')
router.register(r'ltc/f2pool/get', views.api_f2_ltc_pub, basename='f2pool_ltc')
router.register(r'ltc/poolin/post', views.api_poolin_ltc_view_set, basename='poolin_ltc post')
router.register(r'ltc/poolin/get', views.api_poolin_ltc_pub, basename='poolin_ltc')
router.register(r'ltc/trustpool/post', views.api_trustpool_ltc_view_set, basename='trustpool_ltc post')
router.register(r'ltc/trustpool/get', views.api_trustpool_ltc_pub, basename='trustpool_ltc')
router.register(r'ltc/viabtc/post', views.api_viabtc_ltc_view_set, basename='viabtc_ltc post')
router.register(r'ltc/viabtc/get', views.api_viabtc_ltc_pub, basename='viabtc_ltc')
router.register(r'ltc/sigmapool/post', views.api_sigmapool_ltc_view_set, basename='sigmapool_ltc post')
router.register(r'ltc/sigmapool/get', views.api_sigmapool_ltc_pub, basename='sigmapool_ltc')
router.register(r'ltc/litecoinpool/post', views.api_litecoinpool_ltc_view_set, basename='litecoinpool_ltc post')
router.register(r'ltc/litecoinpool/get', views.api_litecoinpool_ltc_pub, basename='litecoinpool_ltc')




urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html')),
    path('btc/', include('pools.urls')),
    path('eth/', include('pools_eth.urls')),
    path('ltc/', include('pools_ltc.urls')),
    path('api/', include(router.urls)),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('api/doc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
